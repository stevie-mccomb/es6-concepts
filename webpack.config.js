const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
const WebpackNotifierPlugin = require('webpack-notifier');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = [
    {
        entry: ['babel-polyfill', './assets/js/app.js'],
        output: {
            filename: 'public/js/app.js'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['babel-preset-env']
                        }
                    }
                },
            ]
        },
        plugins: [
            new UglifyJsPlugin(),
            new WebpackNotifierPlugin({ title: 'ES6', alwaysNotify: true }),
        ]
    },

    {
        entry: './assets/sass/app.scss',
        output: {
            filename: 'public/css/app.css'
        },
        module: {
            rules: [
                {
                    test: /\.(sass|scss)$/,
                    exclude: /(node_modules|bower_components)/,
                    use: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
                },
            ]
        },
        plugins: [
            new ExtractTextPlugin({ filename: 'public/css/app.css', allChunks: true }),
            new WebpackNotifierPlugin({ title: 'ES6', alwaysNotify: true }),
        ]
    }
];
