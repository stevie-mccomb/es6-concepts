import * as Controller from './libraries/Controller';

require('./bootstrap');

jQuery(document).ready(() => {
    let el = jQuery('#app');

    new Game(el);

    let obj1 = { name: 'One' };
    let obj2 = { name: 'Two' };
    let obj3 = { name: 'Three' };

    let set = new Set([obj1, obj2, obj3]);
    let map = new Map([['wat', obj1], ['derp', obj2], ['test', obj3]]);

    set.add('wat');
    map.set('wat', 'wat');
    map.set('hmm', 'nope');

    set.forEach((obj) => { console.log(obj); });
    map.forEach((obj) => { console.log(obj); });
});
