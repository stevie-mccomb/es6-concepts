window.axios = require('axios');

window.$ = window.jQuery = require('jquery');

window.GameObject = require('./classes/GameObject');

window.Controller = require('./libraries/Controller');
window.Game = require('./classes/Game');
window.Stage = require('./classes/Stage');

window.Hero = require('./classes/Hero');
window.Wall = require('./classes/Wall');
window.Pickup = require('./classes/Pickup');

window.Fibonacci = require('./classes/Fibonacci');
