const jQuery = require('jquery');

export const W = 'w';
export const A = 'a';
export const S = 's';
export const D = 'd';
export const ARROW_UP = 'arrow-up';
export const ARROW_LEFT = 'arrow-left';
export const ARROW_DOWN = 'arrow-right';
export const ARROW_RIGHT = 'arrow-right';
export const DPAD_UP = 'd-pad-up';
export const DPAD_LEFT = 'd-pad-left';
export const DPAD_BOTTOM = 'd-pad-bottom';
export const DPAD_RIGHT = 'd-pad-right';
export const TRIANGLE = 'triangle';
export const SQUARE = 'square';
export const CIRCLE = 'circle';
export const CROSS = 'cross';
export const SELECT = 'select';
export const START = 'start';
export const L1 = 'l1';
export const L2 = 'l2';
export const L3 = 'l3';
export const R1 = 'r1';
export const R2 = 'r2';
export const R3 = 'r3';

export let inputs = [];

export function isUp(input) { return inputs.indexOf(input) < 0; }
export function isDown(input) { return inputs.indexOf(input) >= 0; }

jQuery(document).on('keydown', (e) => {
    let input = e.key.toLowerCase();
    let index = inputs.indexOf(input);

    if (index < 0) inputs.push(input);
});

jQuery(document).on('keyup', (e) => {
    let input = e.key.toLowerCase();
    let index = inputs.indexOf(input);

    if (index >= 0) inputs.splice(index, 1);
});
