class Wall extends GameObject
{
    constructor(x = 0, y = 0, width = 32, height = 32)
    {
        super();

        Wall.instances.push(this);

        this.rigid = true;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = '#333';
    }

    render(deltaTime)
    {
        Stage.instance.context.fillStyle = this.color;
        Stage.instance.context.fillRect(this.x, this.y, this.width, this.height);
    }

    destroy()
    {
        super.destroy();

        let index = Wall.instances.indexOf(this);

        if (index >= 0) Wall.instances.splice(index, 1);
    }
}

Wall.instances = [];

module.exports = Wall;
