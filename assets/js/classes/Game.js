class Game
{
    constructor(el)
    {
        if (Game.instance) Game.instance.destroy();
        Game.instance = this;

        this.element = el;
        this.lastUpdated = 0;

        new Stage();

        new Wall(32, 32, 640, 32);
        new Wall(Stage.instance.width - 64, 64, 32, 144);
        new Hero();
        new Pickup(96, 96, 32, 48);

        requestAnimationFrame(this.loop.bind(this));
    }

    loop(timestamp)
    {
        let deltaTime = timestamp - this.lastUpdated;

        this.update(deltaTime);
        this.render(deltaTime);

        this.lastUpdated = timestamp;

        requestAnimationFrame(this.loop.bind(this));
    }

    update(deltaTime)
    {
        Stage.instance.update(deltaTime);

        for (let instance of GameObject.instances) instance.update(deltaTime);
    }

    render(deltaTime)
    {
        Stage.instance.render(deltaTime);

        for (let instance of GameObject.instances) instance.render(deltaTime);
    }

    destroy()
    {
        Game.instance = undefined;
    }

    get width()
    {
        return this.element.outerWidth();
    }
}

module.exports = Game;
