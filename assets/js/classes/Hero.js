class Hero extends GameObject
{
    constructor()
    {
        super();

        if (Hero.instance) Hero.instance.destroy();
        Hero.instance = this;

        this.width = 32;
        this.height = 32;
        this.speed = 8;
        this.color = '#00a6ef';
        this.poisonColor = '#0e0';
        this.poisoned = false;
        this.poisonSpeedEffect = (this.speed / 2);

        this.x = (Stage.instance.width / 2) - (this.width / 2);
        this.y = (Stage.instance.height / 2) - (this.height / 2);
    }

    update(deltaTime)
    {
        super.update(deltaTime);

        let speed = this.poisoned ? this.speed - this.poisonSpeedEffect : this.speed;

        if (Controller.isDown(Controller.W)) this.y -= speed;
        if (Controller.isDown(Controller.A)) this.x -= speed;
        if (Controller.isDown(Controller.S)) this.y += speed;
        if (Controller.isDown(Controller.D)) this.x += speed;

        if (this.top < Stage.instance.top) this.top = Stage.instance.top;
        if (this.left < Stage.instance.left) this.left = Stage.instance.left;
        if (this.bottom > Stage.instance.bottom) this.bottom = Stage.instance.bottom;
        if (this.right > Stage.instance.right) this.right = Stage.instance.right;

        for (let wall of Wall.instances) {
            let collidingSide = this.isColliding(wall);

            if (collidingSide === 'top' && this.top < wall.bottom) this.top = wall.bottom;
            if (collidingSide === 'left' && this.left < wall.right) this.left = wall.right;
            if (collidingSide === 'bottom' && this.bottom > wall.top) this.bottom = wall.top;
            if (collidingSide === 'right' && this.right > wall.left) this.right = wall.left;
        }

        for (let pickup of Pickup.instances) {
            if (this.isColliding(pickup)) {
                this.poisoned = true;

                setTimeout(() => {
                    this.poisoned = false;
                }, pickup.life);

                pickup.destroy();
            }
        }
    }

    render(deltaTime)
    {
        super.render(deltaTime);

        let color = this.poisoned ? this.poisonColor : this.color;

        Stage.instance.context.fillStyle = color;
        Stage.instance.context.fillRect(this.x, this.y, this.width, this.height);
    }

    destroy()
    {
        super.destroy();

        Hero.instance = undefined;
    }
}

module.exports = Hero;
