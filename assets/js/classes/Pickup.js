class Pickup extends GameObject
{
    constructor(x = 0, y = 0, width = 16, height = 16)
    {
        super();

        Pickup.instances.push(this);

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.life = 2500;
    }

    render(deltaTime)
    {
        Stage.instance.context.fillStyle = 'red';
        Stage.instance.context.fillRect(this.x, this.y, this.width, this.height);
    }

    destroy()
    {
        super.destroy();

        let index = Pickup.instances.indexOf(this);

        if (index >= 0) Pickup.instances.splice(index, 1);
    }
}

Pickup.instances = [];

module.exports = Pickup;
