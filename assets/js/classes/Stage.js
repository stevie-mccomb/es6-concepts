class Stage
{
    constructor()
    {
        if (Stage.instance) Stage.instance.destroy();
        Stage.instance = this;

        this.element = jQuery('<canvas class="stage"></canvas>');
        Game.instance.element.append(this.element);
        this.context = this.element.get(0).getContext('2d');

        this.element.attr({ width: this.width, height: this.height });
    }

    update(deltaTime)
    {
        //
    }

    render(deltaTime)
    {
        this.context.clearRect(0, 0, this.width, this.height);
    }

    destroy()
    {
        Stage.instance = undefined;
    }

    get width()
    {
        return this.element.outerWidth();
    }

    get height()
    {
        return this.element.outerHeight();
    }

    get top()
    {
        return 0;
    }

    get left()
    {
        return 0;
    }

    get bottom()
    {
        return this.top + this.height;
    }

    get right()
    {
        return this.left + this.width;
    }
}

module.exports = Stage;
