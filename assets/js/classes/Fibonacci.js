class Fibonacci
{
    constructor()
    {
        this.element = jQuery('#fibonacci');

        let fib = {
            [Symbol.iterator]: function*() {
                var pre = 0, cur = 1;
                for (;;) {
                    var temp = pre;
                    pre = cur;
                    cur += temp;
                    yield cur;
                }
            }
        };

        for (let n of fib) {
            if (n > 999999999999) break;
            this.element.get(0).innerHTML += n += ', ';
        }
    }
}

module.exports = Fibonacci;
